//Libraries to be included
#include "Arduino.h"
#include <Lorro_BQ25703A.h>

//Default address for device. Note, it is without read/write bit. When read with analyser,
//this will appear 1 bit shifted to the left
#define BQ25703ADevaddr    0x6B

//Initialise the device and library
Lorro_BQ25703A BQ25703A;
const byte Lorro_BQ25703A::BQ25703Aaddr = BQ25703ADevaddr;

//Instantiate with reference to global set
extern Lorro_BQ25703A::Regt BQ25703Areg;

uint32_t previousMillis;
uint16_t loopInterval = 60000;

#define EN_OTG 5
#define ILIM_HZ 3

// OTG settings
#define MAX_BATTERY_VOLTAGE   4192    // 1S: 4192 | 2S: 8400 | 3S: 12592 | 4S: 16800
#define OTG_VOLTAGE           8000    // 4480 - 20800 mV    (64 mV resolution)
#define OTG_MAX_CURRENT       6350    // 0    - 6350  mA    (50 mA resolution)    When this limit is reached, OTG is turned off.

void setup() {

  Serial.begin(115200);
  pinMode(EN_OTG, OUTPUT);
  pinMode(ILIM_HZ, OUTPUT);

  digitalWrite(ILIM_HZ, HIGH); // Exit High-Z mode and allow max current. Current limit is thus set by REG0x0F/0E.

  //Set the watchdog timer to not have a timeout
  BQ25703Areg.chargeOption0.set_WDTMR_ADJ( 0 );
  BQ25703Areg.chargeOption0.set_EN_LWPWR( 0 ); // this disables low power mode and so enable more functions, including ADC
  BQ25703Areg.chargeOption0.set_EN_LDO( 0 ); //  Skip precharge mode when battery voltage is below minSystemVoltage()
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption0 );
  delay( 15 );

  //Set the ADC on IBAT and PSYS to record values
  //When changing bitfield values, call the writeRegEx function
  //This is so you can change all the bits you want before sending out the byte.
  BQ25703Areg.chargeOption1.set_EN_IBAT( 1 );
  BQ25703Areg.chargeOption1.set_EN_PSYS( 1 );
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption1 );
  delay( 15 );

  //Set ADC to make continuous readings. (uses more power)
  BQ25703Areg.aDCOption.set_ADC_CONV( 1 );
  //Set individual ADC registers to read. All have default off.
  BQ25703Areg.aDCOption.set_EN_ADC_VBUS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_PSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_IDCHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_ICHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VBAT( 1 );
  //Once bits have been twiddled, send bytes to device
  BQ25703A.writeRegEx( BQ25703Areg.aDCOption );
  delay( 15 );
  
  // Setup OTG (H8.3.3 datasheet)
  /*
   * set valid voltage in REG0x05/04() (maxChargeVoltage)
   * OTG output voltage is set in REG0x07/06() (oTGVoltage)
   * OTG output current is set in REG0x09/08() (oTGCurrent)
   * EN_OTG pin is HIGH
   * REG0x35[4] = 1 (chargeOption3)
   * Vbus is below V(vbus_uvlo) = 2.4V ???????
   */
  BQ25703Areg.maxChargeVoltage.set_voltage( MAX_BATTERY_VOLTAGE );
  BQ25703Areg.oTGVoltage.set_voltage( OTG_VOLTAGE );                  
  BQ25703Areg.oTGCurrent.set_current( OTG_MAX_CURRENT );              
  digitalWrite(EN_OTG, HIGH);
  BQ25703Areg.chargeOption3.set_EN_OTG( 1 );
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption3 );

}

void loop() {
  // Empty...
}

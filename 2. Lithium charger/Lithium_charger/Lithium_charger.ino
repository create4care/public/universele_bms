/**
  Example of reading out the ADC registers to ascertain the different system voltages,
  power and current flows. The device needs some registers to be written as their
  default value is returned to after each device reset. There is no flash storage
  on the device, so they have to be written on every start up.

  This library is written to handle writing to and reading from the registers.
  Read the datasheet to get a full understanding of the device registers and an
  example circuit. http://www.ti.com/lit/ds/symlink/bq25703a.pdf
**/

//Libraries to be included
#include "Arduino.h"
#include <Lorro_BQ25703A.h>

//Default address for device. Note, it is without read/write bit. When read with analyser,
//this will appear 1 bit shifted to the left
#define BQ25703ADevaddr    0x6B

//Initialise the device and library
Lorro_BQ25703A BQ25703A;
const byte Lorro_BQ25703A::BQ25703Aaddr = BQ25703ADevaddr;

//Instantiate with reference to global set
extern Lorro_BQ25703A::Regt BQ25703Areg;

uint32_t previousMillis;
uint16_t loopInterval = 60000;

#define EN_OTG 5
#define ILIM_HZ 3

void setup() {

  Serial.begin(115200);
  pinMode(EN_OTG, OUTPUT);
  pinMode(ILIM_HZ, OUTPUT);

//  analogWrite(ILIM_HZ, 0);
  digitalWrite(ILIM_HZ, HIGH);

  //Set the watchdog timer to not have a timeout
  BQ25703Areg.chargeOption0.set_WDTMR_ADJ( 0 );
  BQ25703Areg.chargeOption0.set_EN_LWPWR( 0 ); // this disables low power mode and so enable more functions, including ADC
  BQ25703Areg.chargeOption0.set_EN_LDO( 0 ); //  Skip precharge mode when battery voltage is below minSystemVoltage()
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption0 );
  delay( 15 );

  //Set the ADC on IBAT and PSYS to record values
  //When changing bitfield values, call the writeRegEx function
  //This is so you can change all the bits you want before sending out the byte.
  BQ25703Areg.chargeOption1.set_EN_IBAT( 1 );
  BQ25703Areg.chargeOption1.set_EN_PSYS( 1 );
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption1 );
  delay( 15 );

  //Set ADC to make continuous readings. (uses more power)
  BQ25703Areg.aDCOption.set_ADC_CONV( 1 );
  //Set individual ADC registers to read. All have default off.
  BQ25703Areg.aDCOption.set_EN_ADC_VBUS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_PSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_IDCHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_ICHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VBAT( 1 );
  //Once bits have been twiddled, send bytes to device
  BQ25703A.writeRegEx( BQ25703Areg.aDCOption );
  delay( 15 );

  // CJV: onderstaande toegevoegd

  // charge settings. the charge settings are not automatically chosen based on the battery voltage. so make sure to set this correctly!
  BQ25703Areg.minSystemVoltage.set_voltage( 3584 ); // 1S: 3584 | 2S: 6144 | 3S: 9216 | 4S: 12288
  BQ25703Areg.maxChargeVoltage.set_voltage( 4192 ); // 1S: 4192 | 2S: 8400 | 3S: 12592 | 4S: 16800
  BQ25703Areg.chargeCurrent.set_current( 2500 ); // 1S Emmerich max. 2.5A | 2S Conrad max. 5A | 3S Conrad max. 10A
  delay( 15 );
  
  // Setup OTG (H8.3.3 datasheet)
  /*
   * set valid voltage in REG0x05/04() (maxChargeVoltage)
   * OTG output voltage is set in REG0x07/06() (oTGVoltage)
   * OTG output current is set in REG0x09/08() (oTGCurrent)
   * EN_OTG pin is HIGH
   * REG0x35[4] = 1 (chargeOption3)
   * Vbus is below V(vbus_uvlo) = 2.4V ???????
   */
//  BQ25703Areg.maxChargeVoltage.set_voltage( 4192 );
//  BQ25703Areg.oTGVoltage.set_voltage( 8000 );        // 4480 - 20800 mV
//  BQ25703Areg.oTGCurrent.set_current( 6350 );        // 0    - 6350  mA     When this limit is reached, OTG is turned off.
//  digitalWrite(EN_OTG, HIGH);
//  BQ25703Areg.chargeOption3.set_EN_OTG( 1 );
//  BQ25703A.writeRegEx( BQ25703Areg.chargeOption3 );

  Serial.println("VBAT;IBAT");
  
  for(int i=0; i<10; i++) {
    Serial.print( BQ25703Areg.aDCVSYSVBAT.get_VBAT() );
    Serial.print(";");
    Serial.println( BQ25703Areg.aDCIBAT.get_ICHG() );
    delay(1000);
  }

}

void loop() {

  uint32_t currentMillis = millis();

  if( currentMillis - previousMillis > loopInterval ){
    previousMillis = currentMillis;

//    Serial.print( "Voltage of VBUS: " );
//    Serial.print( BQ25703Areg.aDCVBUSPSYS.get_VBUS() );
//    Serial.println( "mV" );
//    delay( 15 );
//
//    Serial.print( "System power usage: " );
//    Serial.print( BQ25703Areg.aDCVBUSPSYS.get_sysPower() );
//    Serial.println( "W" );
//    delay( 15 );
//
//    Serial.print( "Voltage of VBAT: " );
//    Serial.print( BQ25703Areg.aDCVSYSVBAT.get_VBAT() );
//    Serial.println( "mV" );
//    delay( 15 );

//    Serial.print( "Voltage of VSYS: " );
//    Serial.print( BQ25703Areg.aDCVSYSVBAT.get_VSYS() );
//    Serial.println( "mV" );
//    delay( 15 );
//
//    Serial.print( "Charging current: " );
//    Serial.print( BQ25703Areg.aDCIBAT.get_ICHG() );
//    Serial.println( "mA" );
//    delay( 15 );

//    Serial.print( "Discharging current: " );
//    Serial.print( BQ25703Areg.aDCIBAT.get_IDCHG() );
//    Serial.println( "mA" );
//    delay( 15 );

//    Serial.print( "Mode: fast-charging: " );
//    Serial.println( BQ25703Areg.chargerStatus.IN_FCHRG() );
//    delay( 15 );
//
//    Serial.print( "Mode: pre-charging: " );
//    Serial.println( BQ25703Areg.chargerStatus.IN_PCHRG() );
//    delay( 15 );
//    
//    Serial.print( "Mode: OTG: " );
//    Serial.println( BQ25703Areg.chargerStatus.IN_OTG() );
//    delay( 15 );

    Serial.print( BQ25703Areg.aDCVSYSVBAT.get_VBAT() );
    Serial.print(";");
    Serial.println( BQ25703Areg.aDCIBAT.get_ICHG() );

//    Serial.println();

  }

}

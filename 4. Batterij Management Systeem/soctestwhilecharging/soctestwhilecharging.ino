  #include "Arduino.h"
#include <Lorro_BQ25703A.h>
#include "Wire.h"
#include "bq34z100g1.hpp"
#include <Adafruit_ADS1X15.h>

// ADC definitions
Adafruit_ADS1115 adc;
float cell_1_voltage, cell_2_voltage, cell_3_voltage;

// BQ25703A definitions
#define BQ25703ADevaddr           0x6B
Lorro_BQ25703A BQ25703A;
const byte Lorro_BQ25703A::BQ25703Aaddr = BQ25703ADevaddr;
extern Lorro_BQ25703A::Regt BQ25703Areg;

// BQ34Z100-G1 definitions
BQ34Z100G1 bq_soc;
#define CELLS_IN_SERIES           3     
#define BATTERY_CAPACITY          5000  // mAh
#define BATTERY_ENERGY            55500 // mWh
#define MAX_VOLTAGE_JEITA_T1_T2   4200  // mV
#define MAX_VOLTAGE_JEITA_T2_T3   4200  // mV
#define MAX_VOLTAGE_JEITA_T3_T4   4200  // mV
#define PACK_CONFIGURATION_FLAGS  0b0100100111011001  // Zie paragraaf 7.3.7.1 van datasheet
#define TAPER_CURRENT             100   // mA
#define MIN_TAPER_CAPACITY        25    // mAh 
#define CELL_TAPER_VOLTAGE        100   // mV
#define TAPER_WINDOW              40    // s
#define TCA_SET                   99    // %
#define TCA_CLEAR                 95    // %
#define FC_SET                    100   // %
#define FC_CLEAR                  98    // %
#define DEADBAND                  5     // mA

// I/O pin definitions
#define EN_OTG                    16
#define ILIM_HZ                   17
#define CHRG_OK                   A7
#define PROCHOT                   2
#define ON_OFF_LED                6
#define ON_OFF_BUTTON             3

uint32_t previousMillis;
uint16_t loopInterval = 1000;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  
  pinMode(EN_OTG, OUTPUT);
  pinMode(ILIM_HZ, OUTPUT);
  pinMode(ON_OFF_LED, OUTPUT);
  pinMode(ON_OFF_BUTTON, INPUT_PULLUP);
  pinMode(PROCHOT, INPUT);
  pinMode(CHRG_OK, INPUT);


  digitalWrite(EN_OTG, LOW);
  digitalWrite(ILIM_HZ, LOW);
  
  adc.begin(0x48);

  delay(5000);
  initializeBQ25703A();
  

  Serial.println("VBAT;ICHG;IDCHG;SoC;passedC;remainingC;fullchargeC;timetoempty;timetofull;temperature;cel1;cel2;cel3;CHRG_OK");

  for(int i=0; i<10; i++) {
    measureCellVoltages();
    Serial.print( BQ25703Areg.aDCVSYSVBAT.get_VBAT() );
    Serial.print(";");
    Serial.print( BQ25703Areg.aDCIBAT.get_ICHG() );
    Serial.print(";");
    Serial.print( BQ25703Areg.aDCIBAT.get_IDCHG() );
    Serial.print(";");
    Serial.print(bq_soc.state_of_charge(), DEC );
    Serial.print(";");
    Serial.print(bq_soc.passed_charge(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.remaining_capacity(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.full_charge_capacity(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.average_time_to_empty(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.average_time_to_full(), DEC);
    Serial.print(";");
    Serial.print((bq_soc.temperature() / 10.0) - 273.15, 1);
    Serial.print(";");
    Serial.print(cell_1_voltage, 3);
    Serial.print(";");
    Serial.print(cell_2_voltage, 3);
    Serial.print(";");
    Serial.print(cell_3_voltage, 3);
    Serial.print(";");
    Serial.println(digitalRead(CHRG_OK));
    delay(1000);
  }
}

void loop() {
  uint32_t currentMillis = millis();
  if(!digitalRead(CHRG_OK)){
    digitalWrite(ON_OFF_LED, HIGH);
  } else {
    digitalWrite(ON_OFF_LED, LOW);
  }

  if( currentMillis - previousMillis > loopInterval ){
    previousMillis = currentMillis;
    measureCellVoltages();

    Serial.print( BQ25703Areg.aDCVSYSVBAT.get_VBAT() );
    Serial.print(";");
    Serial.print( BQ25703Areg.aDCIBAT.get_ICHG() );
    Serial.print(";");
    Serial.print( BQ25703Areg.aDCIBAT.get_IDCHG() );
    Serial.print(";");
    Serial.print(bq_soc.state_of_charge(), DEC );
    Serial.print(";");
    Serial.print(bq_soc.passed_charge(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.remaining_capacity(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.full_charge_capacity(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.average_time_to_empty(), DEC);
    Serial.print(";");
    Serial.print(bq_soc.average_time_to_full(), DEC);
    Serial.print(";");
    Serial.print((bq_soc.temperature() / 10.0) - 273.15, 1);
    Serial.print(";");
    Serial.print(cell_1_voltage, 3);
    Serial.print(";");
    Serial.print(cell_2_voltage, 3);
    Serial.print(";");
    Serial.print(cell_3_voltage, 3);
    Serial.print(";");
    Serial.println(digitalRead(CHRG_OK));
  }
}

void initializeBQ25703A(void) {
  digitalWrite(ILIM_HZ, HIGH);

  //Set the watchdog timer to not have a timeout
  BQ25703Areg.chargeOption0.set_WDTMR_ADJ( 0 );
  BQ25703Areg.chargeOption0.set_EN_LWPWR( 0 ); // this disables low power mode and so enable more functions, including ADC
  BQ25703Areg.chargeOption0.set_EN_LDO( 0 ); //  Skip precharge mode when battery voltage is below minSystemVoltage()
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption0 );
  delay( 15 );

  //Set the ADC on IBAT and PSYS to record values
  //When changing bitfield values, call the writeRegEx function
  //This is so you can change all the bits you want before sending out the byte.
  BQ25703Areg.chargeOption1.set_EN_IBAT( 1 );
  BQ25703Areg.chargeOption1.set_EN_PSYS( 1 );
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption1 );
  delay( 15 );

  //Set ADC to make continuous readings. (uses more power)
  BQ25703Areg.aDCOption.set_ADC_CONV( 1 );
  //Set individual ADC registers to read. All have default off.
  BQ25703Areg.aDCOption.set_EN_ADC_VBUS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_PSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_IDCHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_ICHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VBAT( 1 );
  //Once bits have been twiddled, send bytes to device
  BQ25703A.writeRegEx( BQ25703Areg.aDCOption );
  delay( 15 );

  // charge settings. the charge settings are not automatically chosen based on the battery voltage. so make sure to set this correctly!
  BQ25703Areg.minSystemVoltage.set_voltage( 9216 ); // 1S: 3584 | 2S: 6144 | 3S: 9216 | 4S: 12288
  BQ25703Areg.maxChargeVoltage.set_voltage( 12592 ); // 1S: 4192 | 2S: 8400 | 3S: 12592 | 4S: 16800
  BQ25703Areg.chargeCurrent.set_current( 7000 ); // 1S Emmerich max. 2.5A | 2S Conrad max. 5A | 3S Conrad max. 10A
  delay( 15 );
}

void measureCellVoltages(void) {
  cell_1_voltage = adc.computeVolts(adc.readADC_SingleEnded(2));
  cell_2_voltage = adc.computeVolts(adc.readADC_SingleEnded(1));
  cell_3_voltage = adc.computeVolts(adc.readADC_SingleEnded(0));
}

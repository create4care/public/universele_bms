/*
 * Possible improvements:
 * - Use "number_of_cells_connected" to configure the BMS
 */
 
#include "Arduino.h"
#include <Lorro_BQ25703A.h>
#include "Wire.h"
#include "bq34z100g1.hpp" // NOTE: Use the library changed by C.J. Veenema, see also: Afstudeerstageverslag Batterij Management Systeem, Bijlage J, H3.4.1
#include <Adafruit_ADS1X15.h>
#include <EEPROM.h> // NOTE: EEPROM is erased when it is flashed via ISP header
#include "enums.h"

void measureCellVoltages(void); 
void initializeBQ25703A(void);
void initializeTimers(void);
void onOffButtonInterrupt(void);
void procHotInterrupt(void);
void updateLEDbar(void);
void update_OnOffLED(void);
void startCharging(void);
void stopCharging(void);
void checkIfTemperatureIsSafeToCharge(void);
void bq_unsealed(void);
void bq_write_reg(uint8_t addr, uint8_t val);
void bq_read_flash_block(uint8_t sub_class, uint8_t offset);
uint8_t bq_flash_block_checksum(void);
void setVoltageDeviderBQ34Z100(void); 

// EEPROM variables
/*
 * EEPROM adresses:
 * 0: DO_NOT_CHANGE_SETTINGS
 */
#define DO_NOT_CHANGE_SETTINGS      123     // This makes sure the settings are loaded once, and not every powercycle. The functionality is based on EEPROM: if it changes the new settings are loaded. If sketch is programmed via ISP, EEPROM is cleared, so after uploaden via ISP, new settings are loaded.

// ADC definitions
Adafruit_ADS1115 adc;
uint16_t cell_1_voltage, cell_2_voltage, cell_3_voltage; // millivolt
uint8_t number_of_cells_connected = 0;
#define VOLTAGE_THRESHOLD_CELL_CONNECTED  250 // mV

// BQ25703A definitions
#define BQ25703ADevaddr             0x6B
Lorro_BQ25703A BQ25703A;
const byte Lorro_BQ25703A::BQ25703Aaddr = BQ25703ADevaddr;
extern Lorro_BQ25703A::Regt BQ25703Areg;
#define BQ25703_MIN_SYS_VOLTAGE     6144     // mV  1S: 3584 | 2S: 6144 | 3S: 9216  | 4S: 12288
#define BQ25703_MAX_CHARGE_VOLTAGE  8400     // mV  1S: 4192 | 2S: 8400 | 3S: 12592 | 4S: 16800
#define BQ25703_MAX_CHARGE_CURRENT  5000     // mA
#define MAX_BATT_TEMP               45       // °C

// BQ34Z100-G1 definitions
//#define CONFIGURE_BQ34Z100 // Only need to execute once (or when battery pack changes)
BQ34Z100G1 bq_soc;
#define CELLS_IN_SERIES           2     
#define BATTERY_CAPACITY          5000  // mAh
#define BATTERY_ENERGY            37000 // mWh
#define MAX_VOLTAGE_JEITA_T1_T2   4200  // mV
#define MAX_VOLTAGE_JEITA_T2_T3   4200  // mV
#define MAX_VOLTAGE_JEITA_T3_T4   4200  // mV
#define PACK_CONFIGURATION_FLAGS  0b0000100111100001  // Zie paragraaf 7.3.7.1 van datasheet
#define TAPER_CURRENT             100   // mA
#define MIN_TAPER_CAPACITY        25    // mAh 
#define CELL_TAPER_VOLTAGE        100   // mV
#define TAPER_WINDOW              40    // s
#define TCA_SET                   99    // %
#define TCA_CLEAR                 95    // %
#define FC_SET                    100   // %
#define FC_CLEAR                  98    // %
#define DEADBAND                  5     // mA
uint8_t bq_flash_block_data[32];        // This array is used by the bq_X functions to control the BQ34Z100

// I/O pin definitions
#define LEDBAR_BRIGHTNESS         5
#define ON_OFF_LED                6
#define ON_OFF_BUTTON             3
#define LED_20                    14
#define LED_40                    15
#define LED_60                    13
#define LED_80                    12
#define LED_100                   11
#define LDR                       A6
#define BALANCE_CELL_1            10
#define BALANCE_CELL_2            9
#define BALANCE_CELL_3            8
#define EN_OTG                    16
#define ILIM_HZ                   17
#define CHRG_OK                   A7
#define PROCHOT                   2
#define ON_OFF_SWITCH             7

// Variables
#define TIME_BEFORE_LONGPRESS     2000  // ms   Time before a longpress on the on/off button is detected
uint8_t ambient_light = 0;
uint8_t state_of_charge = 0;
bool battery_is_charging = false;

unsigned long on_off_button_pressed_time = 0;
unsigned long on_off_button_released_time = 0;
bool on_off_button_pressed = false;
bool on_off_button_released = false;
bool on_off_button_shortpress = false;
bool on_off_button_longpress = false;

bool send_data_over_i2c = false;

system_states system_status = off;
system_states system_status_old = off;

int chrg_ok_state = 0;
int chrg_ok_state_old = 0;

void setup() 
{
  Serial.begin(115200);
  Wire.begin();
  adc.begin(0x48);
  initializeTimers();

  if(EEPROM.read(0) == DO_NOT_CHANGE_SETTINGS)
  {
    // Do nothing, settings are already loaded
  }
  else
  {
    EEPROM.write(0, DO_NOT_CHANGE_SETTINGS);
    Serial.println("settings updated");
    bq_soc.update_design_capacity(BATTERY_CAPACITY);
    bq_soc.update_q_max(BATTERY_CAPACITY);
    bq_soc.update_design_energy(BATTERY_ENERGY);
    bq_soc.update_number_of_series_cells(CELLS_IN_SERIES);
    bq_soc.update_pack_configuration(PACK_CONFIGURATION_FLAGS);
    setVoltageDeviderBQ34Z100();
    
//    bq_soc.update_cell_charge_voltage_range(MAX_VOLTAGE_JEITA_T1_T2, MAX_VOLTAGE_JEITA_T2_T3, MAX_VOLTAGE_JEITA_T3_T4); // not necessary for basic usage (use default values)
//    bq_soc.update_charge_termination_parameters(TAPER_CURRENT, MIN_TAPER_CAPACITY, CELL_TAPER_VOLTAGE, TAPER_WINDOW, TCA_SET, TCA_CLEAR, FC_SET, FC_CLEAR); // not necessary for basic usage (use default values)
//    bq_soc.calibrate_cc_offset(); // not necessary for basic usage (use default values)
//    bq_soc.calibrate_board_offset(); // not necessary for basic usage (use default values)
//    !see_commend!bq_soc.calibrate_voltage_divider(APPLIED_VOLTAGE, CELLS_IN_SERIES); // for the BMS, write 15095 to the Voltage devider register instead of calibrating every single device. This is done by setVoltageDeviderBQ34Z100();
//    bq_soc.calibrate_sense_resistor(APPLIED_CURRENT); // not necessary for basic usage (use default values)
//    bq_soc.set_current_deadband(DEADBAND); // not necessary for basic usage (use default values)
//    !caution!bq_soc.ready(); // DO NOT EXECUTE THIS COMMAND, SINCE THIS CAN NOT BE UNDONE! It enables the Impedance Tracking algorithm
  }
  
  pinMode(LEDBAR_BRIGHTNESS, OUTPUT);
  pinMode(ON_OFF_LED, OUTPUT);
  pinMode(LED_20, OUTPUT);
  pinMode(LED_40, OUTPUT);
  pinMode(LED_60, OUTPUT);
  pinMode(LED_80, OUTPUT);
  pinMode(LED_100, OUTPUT);
  pinMode(ON_OFF_BUTTON, INPUT_PULLUP);
  pinMode(LDR, INPUT);
  pinMode(BALANCE_CELL_1, OUTPUT);
  pinMode(BALANCE_CELL_2, OUTPUT);
  pinMode(BALANCE_CELL_3, OUTPUT);
  pinMode(EN_OTG, OUTPUT);
  pinMode(ILIM_HZ, OUTPUT);
  pinMode(PROCHOT, INPUT_PULLUP);
  pinMode(CHRG_OK, INPUT_PULLUP);
  pinMode(ON_OFF_SWITCH, OUTPUT);

  digitalWrite(LEDBAR_BRIGHTNESS, LOW);
  digitalWrite(ON_OFF_LED, LOW);
  digitalWrite(LED_20, LOW);
  digitalWrite(LED_40, LOW);
  digitalWrite(LED_60, LOW);
  digitalWrite(LED_80, LOW);
  digitalWrite(LED_100, LOW);
  digitalWrite(BALANCE_CELL_1, LOW);
  digitalWrite(BALANCE_CELL_2, LOW);
  digitalWrite(BALANCE_CELL_3, LOW);
  digitalWrite(EN_OTG, LOW);
  digitalWrite(ILIM_HZ, LOW);
  digitalWrite(ON_OFF_SWITCH, LOW);
  
  attachInterrupt(digitalPinToInterrupt(ON_OFF_BUTTON), onOffButtonInterrupt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PROCHOT), procHotInterrupt, FALLING);
  
  initializeBQ25703A();
}

void loop() {
//  Serial.println(system_status);
  if(on_off_button_shortpress)
  {
    if(system_status == off)
    {
//      Serial.println("from OFF to BOOTING");
      system_status = booting;
      digitalWrite(ON_OFF_SWITCH, HIGH);
      delay(50);
      digitalWrite(ON_OFF_SWITCH, LOW);
    }
    else if(system_status == on)
    {
//      Serial.println("from ON to STANDBY");
      Wire.beginTransmission(9); 
      Wire.write("CMD:");   
      Wire.write("STDBY");           
      Wire.endTransmission();
      delay(50);
      system_status = standby;
    }
    else if(system_status == standby)
    {
//      Serial.println("from STANDBY to ON");
      Wire.beginTransmission(9); 
      Wire.write("CMD:");   
      Wire.write("ON");           
      Wire.endTransmission();
      delay(50);
      system_status = on;    
    }
    on_off_button_shortpress = false;
  }
  if(on_off_button_longpress)
  {
    Wire.beginTransmission(9); 
    Wire.write("CMD:");   
    Wire.write("OFF");           
    Wire.endTransmission();
    delay(50);
    system_status = shuttingdown;
    on_off_button_longpress = false;
  }
  if(send_data_over_i2c) // This is true twice a second. Using I2C inside a timer interrupt is difficult.
  {
    //send data to C4C system
    char str[10]; 
    int vbat = BQ25703Areg.aDCVSYSVBAT.get_VBAT();
    int ichg = BQ25703Areg.aDCIBAT.get_ICHG();
    int idchg = BQ25703Areg.aDCIBAT.get_IDCHG(); 
    int ttf = bq_soc.average_time_to_full();
    int tte = bq_soc.average_time_to_empty();
    state_of_charge = bq_soc.state_of_charge();

    measureCellVoltages(); // Voltages are stored in cell_1_voltage, cell_2_voltage and cell_3_voltage
    checkIfTemperatureIsSafeToCharge(); // The charging is terminated if the temperature (NTC) is higher than 45 °C
    if(ichg) { battery_is_charging = true; } else { battery_is_charging = false; }

    // State of Charge
    Wire.beginTransmission(9); 
    Wire.write("SoC:");
    sprintf(str,"%d", state_of_charge);
    Wire.write(str);           
    Wire.endTransmission();
    delay(50);

    // Time to empty
    Wire.beginTransmission(9); 
    Wire.write("TTE:");
    sprintf(str,"%d", tte);
    Wire.write(str);           
    Wire.endTransmission();
    delay(50);
    
    // Time to full
    Wire.beginTransmission(9); 
    Wire.write("TTF:");
    sprintf(str,"%d", ttf);
    Wire.write(str);           
    Wire.endTransmission();
    delay(50);
    
    // Request system status
    Wire.beginTransmission(9); 
    Wire.write("CMD:");   
    Wire.write("PING");           
    Wire.endTransmission();
    Wire.requestFrom(9, 1);
    char c = 'F'; // If no reply, the Create4Care system is likely off
    while (Wire.available()) { // slave may send less than requested
      c = Wire.read(); // receive a byte as character
    } 
    if(c == 'N' and system_status != shuttingdown) { system_status = on; }
    if(c == 'S' and system_status != shuttingdown) { system_status = standby; }
    if(c == 'F' and system_status != booting) { system_status = off; }
    if(system_status != system_status_old) { update_OnOffLED(); }
    system_status_old = system_status;

    // Check if a valid charger is connected. If so and the charging has not started yet, start the charging process.
    if(analogRead(CHRG_OK) > 512) { chrg_ok_state = 1; } else { chrg_ok_state = 0; }
    if(chrg_ok_state == 1 and chrg_ok_state_old == 0) { startCharging(); }
    chrg_ok_state_old = chrg_ok_state;
    
    send_data_over_i2c = false; // Reset flag

    // Debugging purposes:
    Serial.print("Voltage:             "); Serial.println(bq_soc.voltage(), DEC);
    Serial.print("Current:             "); Serial.println(bq_soc.current(), DEC);
    Serial.print("Passed charge:       "); Serial.println(bq_soc.passed_charge(), DEC);
    Serial.print("Capacity:            "); Serial.println(bq_soc.full_charge_capacity(), DEC);
    Serial.print("Remaining capacity:  "); Serial.println(bq_soc.remaining_capacity(), DEC);
    Serial.print("Full charge cap.:    "); Serial.println(bq_soc.full_charge_capacity(), DEC);
    Serial.print("Temperature:         "); Serial.println((bq_soc.temperature() / 10.0) - 273.15, DEC);
    Serial.print("Internal Temp.:      "); Serial.println((bq_soc.internal_temperature() / 10.0) - 273.15, DEC);
    Serial.print("Time to empty:       "); Serial.println(bq_soc.average_time_to_empty(), DEC);
    Serial.print("Time to full:        "); Serial.println(bq_soc.average_time_to_full(), DEC);
    Serial.print("SoC:                 "); Serial.println(bq_soc.state_of_charge(), DEC);
    Serial.print("CHRG_OK:             "); Serial.println(analogRead(CHRG_OK));
    Serial.print("Cell 1 voltage:      "); Serial.println(cell_1_voltage);
    Serial.print("Cell 2 voltage:      "); Serial.println(cell_2_voltage);
    Serial.print("Cell 3 voltage:      "); Serial.println(cell_3_voltage);
    Serial.print("Cells connected:     "); Serial.println(number_of_cells_connected);
    Serial.println();
  }
}

void measureCellVoltages(void) 
{
  cell_1_voltage = adc.computeVolts(adc.readADC_SingleEnded(2)) * 1000;
  cell_2_voltage = adc.computeVolts(adc.readADC_SingleEnded(1)) * 1000;
  cell_3_voltage = adc.computeVolts(adc.readADC_SingleEnded(0)) * 1000;

  number_of_cells_connected = 0;
  if(cell_1_voltage > VOLTAGE_THRESHOLD_CELL_CONNECTED)
  {
    number_of_cells_connected++;
    if(cell_2_voltage > VOLTAGE_THRESHOLD_CELL_CONNECTED)
    {
      number_of_cells_connected++;
      if(cell_3_voltage > VOLTAGE_THRESHOLD_CELL_CONNECTED)
      {
        number_of_cells_connected++;
      }
    }
  }
}

void initializeBQ25703A(void) 
{
  digitalWrite(ILIM_HZ, HIGH);

  //Set the watchdog timer to not have a timeout
  BQ25703Areg.chargeOption0.set_WDTMR_ADJ( 0 );
  BQ25703Areg.chargeOption0.set_EN_LWPWR( 0 ); // this disables low power mode and so enable more functions, including ADC
  BQ25703Areg.chargeOption0.set_EN_LDO( 0 ); //  Skip precharge mode when battery voltage is below minSystemVoltage()
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption0 );
  delay( 15 );

  //Set the ADC on IBAT and PSYS to record values
  //When changing bitfield values, call the writeRegEx function
  //This is so you can change all the bits you want before sending out the byte.
  BQ25703Areg.chargeOption1.set_EN_IBAT( 1 );
  BQ25703Areg.chargeOption1.set_EN_PSYS( 1 );
  BQ25703A.writeRegEx( BQ25703Areg.chargeOption1 );
  delay( 15 );

  //Set ADC to make continuous readings. (uses more power)
  BQ25703Areg.aDCOption.set_ADC_CONV( 1 );
  //Set individual ADC registers to read. All have default off.
  BQ25703Areg.aDCOption.set_EN_ADC_VBUS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_PSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_IDCHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_ICHG( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VSYS( 1 );
  BQ25703Areg.aDCOption.set_EN_ADC_VBAT( 1 );
  //Once bits have been twiddled, send bytes to device
  BQ25703A.writeRegEx( BQ25703Areg.aDCOption );
  delay( 15 );

  // charge settings. the charge settings are not automatically chosen based on the battery voltage. so make sure to set this correctly!
  BQ25703Areg.minSystemVoltage.set_voltage( BQ25703_MIN_SYS_VOLTAGE ); // 1S: 3584 | 2S: 6144 | 3S: 9216 | 4S: 12288
  BQ25703Areg.maxChargeVoltage.set_voltage( BQ25703_MAX_CHARGE_VOLTAGE ); // 1S: 4192 | 2S: 8400 | 3S: 12592 | 4S: 16800
  BQ25703Areg.chargeCurrent.set_current( BQ25703_MAX_CHARGE_CURRENT ); // 1S Emmerich max. 2.5A | 2S Conrad max. 5A | 3S Conrad max. 10A
  delay( 15 );
}

void initializeTimers(void) 
{
  cli();

  // Use Timer 1 as a 4Hz interrupt generator
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;
  OCR1A = 31250; // (16MHz / 64 prescaler) / 31250  = 8 Hz
  TCCR1A |= (1 << WGM12); // CTC mode
  TCCR1B |= (1 << CS11) | (1 << CS10);  // CLK / 64 prescaler
  TIMSK1 |= (1 << OCIE1A); // turn on the timer interrupt

  // Use Timer 2 to fade on/off led
  TCCR2A = 0;
  TCCR2B = 0;
  TCNT2  = 0;
  OCR2A = 153; // (16MHz / 1024 prescaler) / 153  = 102.124 Hz
  TCCR2A |= (1 << WGM21);
  TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20); // CLK / 1024 prescaler  
  TIMSK2 &= ~(1 << OCIE2A); // turn off the timer interrupt responsible for fading
  
  sei();
}

void onOffButtonInterrupt(void) 
{
  int button_state = PIND & (1 << 3);
  static int button_state_old = 1;
  
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  
  if(interrupt_time - last_interrupt_time > 20) 
  {
    if(button_state == 0 and button_state_old != 0) 
    {
      // button is pressed and debounced
      on_off_button_pressed_time = millis();
      on_off_button_pressed = true;
    } 
    else 
    {
      // button is released and debounced
      on_off_button_released_time = millis();
      on_off_button_released = true;
    }
  }

  last_interrupt_time = interrupt_time;
  button_state_old = button_state;
}

void procHotInterrupt(void) 
{
  // Handle interrupt if BQ25703's PROCHOT pin changes
}

/*
 * timer tasks:
 * - change brightness according to surrounding                   2 Hz
 * - fade on off button led                                       
 * - blink led @ 1 or 2 Hz                                        2 Hz      
 * - read system values like voltage, current, temperature, etc   2 Hz
 */
ISR(TIMER1_COMPA_vect)
{
  unsigned long interrupt_time = millis();
  static bool frequency_devider = false;
  frequency_devider = !frequency_devider;
  
  static uint8_t measured_ambient_light = 0;
  uint8_t ambient_light_old = 0;
  
  measured_ambient_light = (1024 - analogRead(LDR)) / 4;
  if(measured_ambient_light != ambient_light_old) 
  {
    ambient_light = measured_ambient_light;
    analogWrite(LEDBAR_BRIGHTNESS, ambient_light);
  }

  if(battery_is_charging) 
  { 
    if(frequency_devider) 
    {
      updateLEDbar(); // f(led) = 1 Hz
    }
  } else 
  {
    updateLEDbar(); // f(led) = 2 Hz
  }

  if(frequency_devider)
  {
    send_data_over_i2c = true; // send data to C4C system reguraly (2Hz)
    if(system_status == booting or system_status == shuttingdown)
    {
      update_OnOffLED(); // Make the on/off led blinking with f=1Hz
    }
  }

  if(on_off_button_pressed) 
  {
    if(interrupt_time - on_off_button_pressed_time >= TIME_BEFORE_LONGPRESS) 
    {
      on_off_button_longpress = true;
      on_off_button_pressed = false;
    }
  }
  if(on_off_button_released) 
  {
    if(on_off_button_released_time - on_off_button_pressed_time < TIME_BEFORE_LONGPRESS) 
    {
      on_off_button_shortpress = true;
      on_off_button_released = false;
      on_off_button_pressed = false;
    }
  }
}
  
ISR(TIMER2_COMPA_vect)
{
  static uint8_t on_off_led_pwm_value = 0;
  static bool on_off_led_pwm_value_increase = false;
  
  if(on_off_led_pwm_value_increase) 
  {
    on_off_led_pwm_value++;
    analogWrite(ON_OFF_LED, on_off_led_pwm_value);
    if(on_off_led_pwm_value == 255) 
    {
      on_off_led_pwm_value_increase = false;
    }
  } 
  else 
  {
    on_off_led_pwm_value--;
    analogWrite(ON_OFF_LED, on_off_led_pwm_value);
    if(on_off_led_pwm_value == 0) 
    {
      on_off_led_pwm_value_increase = true;
    }
  }
}

void updateLEDbar(void) 
{
  if(state_of_charge >= 0 && state_of_charge < 10) 
  {
   digitalWrite(LED_100, LOW);
   digitalWrite(LED_80, LOW);
   digitalWrite(LED_60, LOW);
   digitalWrite(LED_40, LOW);
   digitalWrite(LED_20, !digitalRead(LED_20)); // Toggle led
  }
  if(state_of_charge >= 10 && state_of_charge < 30) 
  {
   digitalWrite(LED_100, LOW);
   digitalWrite(LED_80, LOW);
   digitalWrite(LED_60, LOW);
   digitalWrite(LED_40, LOW);
   if(battery_is_charging) { digitalWrite(LED_20, !digitalRead(LED_20)); } // Toggle led
   else { digitalWrite(LED_20, HIGH); }
  }
  if(state_of_charge >= 30 && state_of_charge < 50) 
  {
   digitalWrite(LED_100, LOW);
   digitalWrite(LED_80, LOW);
   digitalWrite(LED_60, LOW);
   if(battery_is_charging) { digitalWrite(LED_40, !digitalRead(LED_40)); } // Toggle led
   else { digitalWrite(LED_40, HIGH); }
   digitalWrite(LED_20, HIGH);
  }
  if(state_of_charge >= 50 && state_of_charge < 70) 
  {
   digitalWrite(LED_100, LOW);
   digitalWrite(LED_80, LOW);
   if(battery_is_charging) { digitalWrite(LED_60, !digitalRead(LED_60)); } // Toggle led
   else { digitalWrite(LED_60, HIGH); }
   digitalWrite(LED_40, HIGH);
   digitalWrite(LED_20, HIGH);
  }
  if(state_of_charge >= 70 && state_of_charge < 90) 
  {
   digitalWrite(LED_100, LOW);
   if(battery_is_charging) { digitalWrite(LED_80, !digitalRead(LED_80)); } // Toggle led
   else { digitalWrite(LED_80, HIGH); }
   digitalWrite(LED_60, HIGH);
   digitalWrite(LED_40, HIGH);
   digitalWrite(LED_20, HIGH);
  }
  if(state_of_charge >= 90 && state_of_charge <= 100) 
  {
   if(battery_is_charging) { digitalWrite(LED_100, !digitalRead(LED_100)); } // Toggle led
   else { digitalWrite(LED_100, HIGH); }
   digitalWrite(LED_80, HIGH);
   digitalWrite(LED_60, HIGH);
   digitalWrite(LED_40, HIGH);
   digitalWrite(LED_20, HIGH);
  }
}

void update_OnOffLED(void) 
{
  if(system_status == booting)
  {
    digitalWrite(ON_OFF_LED, !digitalRead(ON_OFF_LED)); // Toggle led
  }
  else if(system_status == on)
  {
    digitalWrite(ON_OFF_LED, HIGH);
  }
  else if(system_status == standby)
  {
    TIMSK2 |= (1 << OCIE2A); // turn on the timer interrupt responsible for fading
  }
  else if(system_status == shuttingdown)
  {
    digitalWrite(ON_OFF_LED, !digitalRead(ON_OFF_LED)); // Toggle led
  }
  else if(system_status == off)
  {
    digitalWrite(ON_OFF_LED, LOW);
  }
  
  if(system_status != standby)
  {
    // Make sure the timer responsible for fading is turned off
    TIMSK2 &= ~(1 << OCIE2A); // turn off the timer interrupt responsible for fading
  }
}

void startCharging(void)
{
  BQ25703Areg.minSystemVoltage.set_voltage( BQ25703_MIN_SYS_VOLTAGE ); // 1S: 3584 | 2S: 6144 | 3S: 9216 | 4S: 12288
  BQ25703Areg.maxChargeVoltage.set_voltage( BQ25703_MAX_CHARGE_VOLTAGE ); // 1S: 4192 | 2S: 8400 | 3S: 12592 | 4S: 16800
  BQ25703Areg.chargeCurrent.set_current( BQ25703_MAX_CHARGE_CURRENT ); // 1S Emmerich max. 2.5A | 2S Conrad max. 5A | 3S Conrad max. 10A
  delay( 15 );
}

void stopCharging(void)
{
  BQ25703Areg.chargeCurrent.set_current( 0 );
  delay( 15 );
}

void checkIfTemperatureIsSafeToCharge(void)
{
  if(battery_is_charging)
  {
    if(((bq_soc.temperature() / 10.0) - 273.15) > MAX_BATT_TEMP)
    {
      stopCharging();
    }
  }
}

// Functions to control BQ34Z100

void bq_unsealed(void) 
{
  Wire.beginTransmission(0x55);
  Wire.write(0x00); // Control
  Wire.write(0x14);
  Wire.write(0x04);
  Wire.endTransmission();
  
  Wire.beginTransmission(0x55);
  Wire.write(0x00); // Control
  Wire.write(0x72);
  Wire.write(0x36);
  Wire.endTransmission();
}

void bq_write_reg(uint8_t addr, uint8_t val) 
{
    Wire.beginTransmission(0x55);
    Wire.write(addr);
    Wire.write(val);
    Wire.endTransmission(true);
}

void bq_read_flash_block(uint8_t sub_class, uint8_t offset) 
{
    bq_write_reg(0x61, 0x00); // Block control
    bq_write_reg(0x3e, sub_class); // Flash class
    bq_write_reg(0x3f, offset / 32); // Flash block
    
    Wire.beginTransmission(0x55);
    Wire.write(0x40); // Block data
    Wire.requestFrom(0x55, 32, true);
    for (uint8_t i = 0; i < 32; i++) 
    {
        bq_flash_block_data[i] = Wire.read(); // Data
    }
}

uint8_t bq_flash_block_checksum(void) 
{
    uint8_t temp = 0;
    for (uint8_t i = 0; i < 32; i++) 
    {
        temp += bq_flash_block_data[i];
    }
    return 255 - temp;
}

void setVoltageDeviderBQ34Z100(void)
{
  // Steps to write a register
  // 1) unseal device
  // 2) read flash block
  // 3) pas betreffende waardes in 32-byte lijst aan
  // 4) schrijf veranderde waardes met bq_write_reg
  // 5) bereken flash block checksum met bq_flash_block_checksum
  // 6) schrijf checksum naar checksum register van het betreffende 32-byte flashblock
  // 7) wacht 150ms (volgens library)

  bq_unsealed();                                  // (1)
  bq_read_flash_block(104, 14);                   // (2)
  bq_flash_block_data[14] = 0x3A;                 // (3)
  bq_flash_block_data[15] = 0xF7;                 // (3)
  bq_write_reg(0x40+14, bq_flash_block_data[14]); // (4) Voltage divider (was 15095 = 0x3AF7)
  bq_write_reg(0x40+15, bq_flash_block_data[15]); // (4) Voltage divider (was 15095 = 0x3AF7)
  bq_write_reg(0x60, bq_flash_block_checksum());  // (5+6)
  delay(150);                                     // (7)
}

#!/usr/bin/env python

# bsc_arduino.py
# 2016-10-31
# Public Domain

"""
This code is an example of using the Pi as an I2C slave device
receiving messages from an Arduino acting as the I2C bus master.

On the Pi

The BSC slave SDA is GPIO 18 (pin 12) and the BSC slave
SCL is GPIO 19 (pin 35).

On the Arduino

SDA is generally A4 and SCL is generally A5.

Make the following connections.

GPIO 18 <--- SDA ---> A4
GPIO 19 <--- SCL ---> A5

You should also add pull-up resistors of 4k7 or so on each of
GPIO 18 and 19 to 3V3.  The software sets the internal pull-ups
which may work reliably enough.

On the Arduino use the following code.

#include <Wire.h>

void setup()
{
   Wire.begin(); // join i2c bus as master
}

char str[17];

int x = 0;

void loop()
{
   sprintf(str, "Message %7d\n", x);
   if (++x > 9999999) x=0;

   Wire.beginTransmission(9); // transmit to device #9
   Wire.write(str);           // sends 16 bytes
   Wire.endTransmission();    // stop transmitting

   delay(50);
}
"""
import time
import pigpio
import tkinter as tk
import tkinter.font as tkFont
import sys
import os

if os.environ.get('DISPLAY', '') == '':
    print('no display found. Using :0.0')
    os.environ.__setitem__(' DISPLAY', ':0.0')

SDA=18
SCL=19

I2C_ADDR=9

system_status = "ON"

fullscreen = False
root = None
frame = None
dfont = None

def i2c(id, tick):
   global pi, system_status

   s, b, d = pi.bsc_i2c(I2C_ADDR)

   if b:
       try:
           if d[::1].decode("utf-8").split(':')[0] == "SoC":
               SoC.set(int(d[::1].decode("utf-8").split(':')[1]))
           if d[::1].decode("utf-8").split(':')[0] == "TTE":
               TTE.set(int(d[::1].decode("utf-8").split(':')[1]))
           if d[::1].decode("utf-8").split(':')[0] == "TTF":
               TTF.set(int(d[::1].decode("utf-8").split(':')[1]))
           if d[::1].decode("utf-8").split(':')[0] == "CMD":
               if d[::1].decode("utf-8").split(':')[1] == "OFF":
                   shut_down()
               if d[::1].decode("utf-8").split(':')[1] == "STDBY":
                   system_status = "SB"
                   hdmi_off()
                   print("received CMD STDBY")
               if d[::1].decode("utf-8").split(':')[1] == "ON":
                   system_status = "ON"
                   hdmi_on()
                   print("received CMD ON")
               if d[::1].decode("utf-8").split(':')[1] == "PING":
                   print('ping request received, status is:')
                   print(system_status)
                   if system_status == "ON":
                       pi.bsc_i2c(I2C_ADDR, "N")
                   if system_status == "SB":
                       pi.bsc_i2c(I2C_ADDR, "S")

           #print(d) 
                  
       except:
           print("Error in IF statement", sys.exc_info())
 
def toggle_fullscreen(event=None):
    global root
    global fullscreen
    fullscreen = not fullscreen
    root.attributes('-fullscreen' , fullscreen)
    resize()

def end_fullscreen(event=None):
    global root
    global fullscreen
    fullscreen = False
    root.attributes('-fullscreen', False)
    resize()
    
def resize(event=None):
    global dfont
    global frame
    new_size = -max(12, int((frame.winfo_height() / 10)))
    dfont.configure(size=new_size)
    
def exit_app(event=None):
    e.cancel()
    pi.bsc_i2c(0) # Disable BSC peripheral
    pi.stop()
    exit()

def shut_down():
    print("shutting down")
    command = "/usr/bin/sudo /sbin/shutdown -h now"
    import subprocess
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print(output)

def hdmi_off():
    print("turning hdmi off")
    command = "/usr/bin/sudo vcgencmd display_power 0"
    import subprocess
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print(output)
    
def hdmi_on():
    print("turning hdmi on")
    command = "/usr/bin/sudo vcgencmd display_power 1"
    import subprocess
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print(output)

pi = pigpio.pi()

if not pi.connected:
    exit()

# Add pull-ups in case external pull-ups haven't been added

pi.set_pull_up_down(SDA, pigpio.PUD_UP)
pi.set_pull_up_down(SCL, pigpio.PUD_UP)

pi.bsc_i2c(I2C_ADDR) # Configure BSC as I2C slave

root = tk.Tk()
root.title("Demo" )

frame = tk.Frame(root)
frame.pack(fill=tk.BOTH, expand=1)

dfont = tkFont.Font(size=-24)

# GUI variables
SoC = tk.IntVar()
SoC.set(0)
TTE = tk.IntVar()
TTE.set(0)
TTF = tk.IntVar()
TTF.set(0)

# Create widgets
label_soc_name = tk.Label(frame, text="SoC:", font=dfont)
label_soc = tk.Label(frame, textvariable=SoC, font=dfont)
label_tte_name = tk.Label(frame, text="Time to empty:", font=dfont)
label_tte = tk.Label(frame, textvariable=TTE, font=dfont)
label_ttf_name = tk.Label(frame, text="Time to full:", font=dfont)
label_ttf = tk.Label(frame, textvariable=TTF, font=dfont)

# Lay out widgets in a grid in the frame
label_soc_name.grid(row=0, column=0, padx=5, pady=5, sticky=tk.E)
label_soc.grid(row=0, column=1, padx=5, pady=5, sticky=tk.E)
label_tte_name.grid(row=1, column=0, padx=5, pady=5, sticky=tk.E)
label_tte.grid(row=1, column=1, padx=5, pady=5, sticky=tk.E)
label_ttf_name.grid(row=2, column=0, padx=5, pady=5, sticky=tk.E)
label_ttf.grid(row=2, column=1, padx=5, pady=5, sticky=tk.E)

# Make it so that the grid cells expand out to fill the window
for i in range(0,3):
    frame.rowconfigure(i, weight=1)
for i in range(0,3):
    frame.columnconfigure(i, weight=1)
    
root.bind('<F11>', toggle_fullscreen)
root.bind('<Escape>', end_fullscreen)
root.bind('<Configure>', resize)
root.bind('<Delete>', exit_app)

# Respond to BSC slave activity

e = pi.event_callback(pigpio.EVENT_BSC, i2c)

toggle_fullscreen()
root.mainloop()

# time.sleep(600)

# e.cancel()

# pi.bsc_i2c(0) # Disable BSC peripheral

# pi.stop()


/*
 * JEITA temperature range:
 *  T1: -10°C
 *  T2: 10°C
 *  T3: 45°C
 *  T4: 55°C
 * 
 * VRAAG:
 *  - Wat doen instellingen voor maximale waarden bij de BQ34Z100? Hij kan niets doen als het fout gaat.
 *  
 * WERKING LIBRARY:
 *  - read_control(): alle functies paragraaf 7.3.1.3 - 7.3.2.32
 *  - Learned_status() & 0b100 = IT_enabled
 *  
 * NOTES:
 *  set:              0b0000100111100001
 *  read:             0b0100100011100101
 *  read (new pack):  0b0100000111011001
 */

#include "Wire.h"
#include "bq34z100g1.hpp"

BQ34Z100G1 bq_soc;

#define CONFIGURE_BQ34Z100

// BQ34Z100 (BATTERY) SETTINGS
#define CELLS_IN_SERIES           2     
#define BATTERY_CAPACITY          5000  // mAh
#define BATTERY_ENERGY            37000 // mWh
#define MAX_VOLTAGE_JEITA_T1_T2   4200  // mV
#define MAX_VOLTAGE_JEITA_T2_T3   4200  // mV
#define MAX_VOLTAGE_JEITA_T3_T4   4200  // mV
#define PACK_CONFIGURATION_FLAGS  0b0000100111100001  // Zie paragraaf 7.3.7.1 van datasheet
#define TAPER_CURRENT             100   // mA
#define MIN_TAPER_CAPACITY        25    // mAh 
#define CELL_TAPER_VOLTAGE        100   // mV
#define TAPER_WINDOW              40    // s
#define TCA_SET                   99    // %
#define TCA_CLEAR                 95    // %
#define FC_SET                    100   // %
#define FC_CLEAR                  98    // %
#define DEADBAND                  5     // mA

// TBV CALIBRATION
#define APPLIED_VOLTAGE           8170  // mV
#define APPLIED_CURRENT           1234     // mA

uint8_t bq_flash_block_data[32]; // Voor eigen functies
uint32_t data_buffer = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);//Start Serial comms
  Wire.begin();

  #ifdef CONFIGURE_BQ34Z100
    Serial.println("Configure settings start");
//    bq_full_access();
//    bq_unsealed();
    
    // Zie ook datasheet paragraaf 8.2.2 voor ontwerp procedure

//    bq_soc.update_design_capacity(BATTERY_CAPACITY);
//    delay(200);
//    Serial.println("Updated design capacity");
//    bq_soc.update_q_max(BATTERY_CAPACITY);
//    delay(200);
//    Serial.println("Updated Q max");
//    bq_soc.update_design_energy(BATTERY_ENERGY);
//    delay(200);
//    Serial.println("Updated design energy");
//    bq_soc.update_cell_charge_voltage_range(MAX_VOLTAGE_JEITA_T1_T2, MAX_VOLTAGE_JEITA_T2_T3, MAX_VOLTAGE_JEITA_T3_T4);
//    delay(200);
//    Serial.println("Set a JEITA profile");
//    bq_soc.update_number_of_series_cells(CELLS_IN_SERIES);
//    delay(200);
//    Serial.println("Set number of series cells");
//    bq_soc.update_pack_configuration(PACK_CONFIGURATION_FLAGS);
//    delay(200);
//    Serial.println("Updated pack configuration flags");
//    bq_soc.update_charge_termination_parameters(TAPER_CURRENT, MIN_TAPER_CAPACITY, CELL_TAPER_VOLTAGE, TAPER_WINDOW, TCA_SET, TCA_CLEAR, FC_SET, FC_CLEAR);
//    delay(200);
//    Serial.println("Updated charge termination parameters");
//    bq_soc.calibrate_cc_offset();
//    delay(200);
//    Serial.println("Calibrated CC offset");
//    bq_soc.calibrate_board_offset();
//    delay(200);
//    Serial.println("Calibrated board offset");
//    bq_soc.calibrate_voltage_divider(APPLIED_VOLTAGE, CELLS_IN_SERIES);
//    delay(200); 
//    Serial.println("Calibrated voltage divider");
//    bq_soc.calibrate_sense_resistor(APPLIED_CURRENT);
//    delay(200);
//    Serial.println("Calibrated current");
//    bq_soc.set_current_deadband(DEADBAND);
//    delay(200);
//    Serial.println("Set current deadband");

    // 1) unseal
    // 2) read flash block
    // 3) pas betreffende waardes in 32-byte lijst aan
    // 4) schrijf veranderde waardes met bq_write_reg
    // 5) bereken flash block checksum met bq_flash_block_checksum
    // 6) schrijf checksum naar checksum register van het betreffende 32-byte flashblock
    // 7) wacht 150ms (volgens library)

    // Volgende code werkt
    bq_unsealed();                                  // (1)
    bq_read_flash_block(104, 14);                   // (2)
    bq_flash_block_data[14] = 0x3A;                 // (3)
    bq_flash_block_data[15] = 0xF7;                 // (3)
    bq_write_reg(0x40+14, bq_flash_block_data[14]); // (4) Voltage divider (was 15095 = 0x3AF7)
    bq_write_reg(0x40+15, bq_flash_block_data[15]); // (4) Voltage divider (was 15095 = 0x3AF7)
    bq_write_reg(0x60, bq_flash_block_checksum());  // (5+6)
    delay(150);                                     // (7)

    bq_unsealed();                                  // (1)
    bq_read_flash_block(104, 0);                    // (2)
//    data_buffer = bq_soc.double_to_xemics(0.71111);
//    bq_flash_block_data[0] = data_buffer >> 24;     // (3)
//    bq_flash_block_data[1] = data_buffer >> 16;     // (3)
//    bq_flash_block_data[2] = data_buffer >> 8;      // (3)
//    bq_flash_block_data[3] = data_buffer;           // (3)
    bq_flash_block_data[0] = 0x7f;                  // (3) default 0x7f71205c value according to https://e2e.ti.com/support/power-management-group/power-management/f/power-management-forum/717379/bq34z100-g1-cc-gain-cc-delta
    bq_flash_block_data[1] = 0x71;                  // (3)
    bq_flash_block_data[2] = 0x20;                  // (3)
    bq_flash_block_data[3] = 0x5c;                  // (3)
    bq_write_reg(0x40+0, bq_flash_block_data[0]);   // (4) CC Gain
    bq_write_reg(0x40+1, bq_flash_block_data[1]);   // (4) CC Gain
    bq_write_reg(0x40+2, bq_flash_block_data[2]);   // (4) CC Gain
    bq_write_reg(0x40+3, bq_flash_block_data[3]);   // (4) CC Gain
    bq_write_reg(0x60, bq_flash_block_checksum());  // (5+6)
    delay(150);                                     // (7)

    bq_unsealed();                                  // (1)
    bq_read_flash_block(104, 4);                    // (2)
//    data_buffer = bq_soc.double_to_xemics(848388);
//    bq_flash_block_data[4] = data_buffer >> 24;     // (3) 
//    bq_flash_block_data[5] = data_buffer >> 16;     // (3)
//    bq_flash_block_data[6] = data_buffer >> 8;      // (3)
//    bq_flash_block_data[7] = data_buffer;           // (3)
    bq_flash_block_data[4] = 0x94;                  // (3) default 0x940898c0 volgens https://e2e.ti.com/support/power-management-group/power-management/f/power-management-forum/504224/bq27510-g2-question-and-datasheet-correction
    bq_flash_block_data[5] = 0x08;                  // (3)
    bq_flash_block_data[6] = 0x98;                  // (3)
    bq_flash_block_data[7] = 0xc0;                  // (3)
    bq_write_reg(0x40+4, bq_flash_block_data[4]);   // (4) CC Delta
    bq_write_reg(0x40+5, bq_flash_block_data[5]);   // (4) CC Delta
    bq_write_reg(0x40+6, bq_flash_block_data[6]);   // (4) CC Delta
    bq_write_reg(0x40+7, bq_flash_block_data[7]);   // (4) CC Delta
    bq_write_reg(0x60, bq_flash_block_checksum());  // (5+6)
    delay(150);                                     // (7)
//
//    bq_unsealed();                                  // (1)
//    bq_read_flash_block(104, 8);                    // (2)
////    bq_flash_block_data[8] = 0b11111011;            // (3) -1200 (volgens datasheet standaard)
////    bq_flash_block_data[9] = 0b01010000;            // (3)
//    bq_flash_block_data[8] = 0x00;                  // (3)
//    bq_flash_block_data[9] = 0x00;                  // (3)
//    bq_write_reg(0x40+8, bq_flash_block_data[8]);   // (4) CC Offset
//    bq_write_reg(0x40+9, bq_flash_block_data[9]);   // (4) CC Offset
//    bq_write_reg(0x60, bq_flash_block_checksum());  // (5+6)
//    delay(150);                                     // (7)

//    bq_unsealed();                                  // (1)
//    bq_read_flash_block(104, 10);                    // (2)
//    bq_flash_block_data[10] = 0x00;                  // (3)
//    bq_write_reg(0x40+10, bq_flash_block_data[10]);   // (4) board offset
//    bq_write_reg(0x60, bq_flash_block_checksum());  // (5+6)
//    delay(150);       
    
    Serial.println("Configure settings stop");
  #endif

////  bq_soc.unsealed();
//  bq_soc.reset();
//  delay(200);
////  bq_soc.sealed();
//  Serial.println("BQ34Z100 is reset");

/*
 * DANGER ZONE. DO NOT EXECUTE bq.soc.ready() !!! THIS WILL LEAVE TEST AND DEVELOPMENT MODE FOREVER!!!
 * zie 7.3.6.10 voor status van IT_enable
 */
//  bq_soc.!!!!ready();
//  delay(200);
//  Serial.println("BQ34Z100 is ready");
/*
 * DANGER ZONE. DO NOT EXECUTE bq.soc.ready() !!! THIS WILL LEAVE TEST AND DEVELOPMENT MODE FOREVER!!!
 */

//  bq_soc.exit_cal();
  delay(200);
}

void loop() {
  Serial.print("Voltage:             "); Serial.println(bq_soc.voltage(), DEC);
  Serial.print("Current:             "); Serial.println(bq_soc.current(), DEC);
  Serial.print("Average Current:     "); Serial.println(bq_soc.average_current(), DEC);
//  Serial.print("Available energy:    "); Serial.println(bq_soc.available_energy(), DEC);
  Serial.print("Average power:       "); Serial.println(bq_soc.average_power(), DEC);
//  Serial.print("Passed charge:       "); Serial.println(bq_soc.passed_charge(), DEC);
//  Serial.print("Capacity:            "); Serial.println(bq_soc.full_charge_capacity(), DEC);
//  Serial.print("Remaining capacity:  "); Serial.println(bq_soc.remaining_capacity(), DEC);
//  Serial.print("Full charge cap.:    "); Serial.println(bq_soc.full_charge_capacity(), DEC);
//  Serial.print("Time to empty:       "); Serial.println(bq_soc.average_time_to_empty(), DEC);
//  Serial.print("Time to full:        "); Serial.println(bq_soc.average_time_to_full(), DEC);
//  Serial.print("SoC:                 "); Serial.println(bq_soc.state_of_charge(), DEC);
//  Serial.print("SoC max error:       "); Serial.println(bq_soc.state_of_charge_max_error(), DEC);
//  Serial.print("SoH:                 "); Serial.println(bq_soc.state_of_health(), DEC);
//  Serial.print("Temperature:         "); Serial.println((bq_soc.temperature() / 10.0) - 273.15, DEC);
//  Serial.print("Internal Temp.:      "); Serial.println((bq_soc.internal_temperature() / 10.0) - 273.15, DEC);
//  Serial.print("Pack configuration:  "); Serial.println(bq_soc.pack_configuration(), BIN);
//  Serial.print("Flags:               "); Serial.println(bq_soc.flags(), BIN);
//  Serial.print("Flags B:             "); Serial.println(bq_soc.flags_b(), BIN);
////  Serial.print("Offset cal:          "); Serial.println(bq_soc.offset_cal(), DEC);
//  Serial.print("CC Offset:           "); Serial.println(bq_soc.cc_offset(), DEC);
//  Serial.print("Board Offset:        "); Serial.println(bq_soc.board_offset(), DEC);
//  Serial.print("Control status:      "); Serial.println(bq_soc.control_status(), BIN);
//  Serial.print("Device type:         "); Serial.println(bq_soc.device_type(), DEC);
//  Serial.print("Serial number:       "); Serial.println(bq_soc.serial_number(), DEC);
//  Serial.print("FW version:          "); Serial.println(bq_soc.fw_version(), DEC);
//  Serial.print("HW version:          "); Serial.println(bq_soc.hw_version(), DEC);
//  Serial.print("Cycle count:         "); Serial.println(bq_soc.cycle_count(), DEC);
//  Serial.print("Reset count:         "); Serial.println(bq_soc.reset_data(), DEC);
//  Serial.print("Chemical ID:         "); Serial.println(bq_soc.chem_id(), DEC);
//  Serial.print("Data flash version:  "); Serial.println(bq_soc.df_version(), DEC);
//  Serial.print("Previous MAC write:  "); Serial.println(bq_soc.prev_macwrite(), DEC);
//  Serial.print("Charge voltage:      "); Serial.println(bq_soc.charge_voltage(), DEC);
//  Serial.print("Charge current:      "); Serial.println(bq_soc.charge_current(), DEC);
//  Serial.print("Design capacity:     "); Serial.println(bq_soc.design_capacity(), DEC);
//  Serial.print("Grid number:         "); Serial.println(bq_soc.grid_number(), DEC);
//  Serial.print("Learned status:      "); Serial.println(bq_soc.learned_status(), BIN);
//  Serial.print("DoD at EoC:          "); Serial.println(bq_soc.dod_at_eoc(), DEC);
//  Serial.print("Q start:             "); Serial.println(bq_soc.q_start(), DEC);
//  Serial.print("True FCC:            "); Serial.println(bq_soc.true_fcc(), DEC);
//  Serial.print("State time:          "); Serial.println(bq_soc.state_time(), DEC);
//  Serial.print("Q max passed Q:      "); Serial.println(bq_soc.q_max_passed_q(), DEC);
//  Serial.print("DoD 0:               "); Serial.println(bq_soc.dod_0(), DEC);
//  Serial.print("DoD 0 time:          "); Serial.println(bq_soc.do_d0_time(), DEC);
//  Serial.print("Q max DoD 0:         "); Serial.println(bq_soc.q_max_dod_0(), DEC);
//  Serial.print("Q max time:          "); Serial.println(bq_soc.q_max_time(), DEC);
  Serial.print("CC Gain:             "); Serial.println(4.768/bq_soc.xemics_to_double(bq_read_cc_gain()), DEC);
  Serial.print("CC Delta:            "); Serial.println(5677445/bq_soc.xemics_to_double(bq_read_cc_delta()), DEC);
  Serial.print("CC Offset:           "); Serial.println(bq_read_cc_offset(), DEC);
  Serial.print("Board Offset:        "); Serial.println(bq_read_board_offset(), DEC);
  Serial.print("Voltage divider:     "); Serial.println(bq_read_voltage_divider(), DEC);
//  Serial.println("\nTESTSECTION:");
//  bq_soc.unsealed();
//  bq_soc.read_flash_block(104, 0);
//
//  uint32_t cc_gain = (uint32_t)bq_soc.flash_block_data[0] << 24;
//  cc_gain |= (uint32_t)bq_soc.flash_block_data[1] << 16;
//  cc_gain |= (uint32_t)bq_soc.flash_block_data[2] << 8;
//  cc_gain |= (uint32_t)bq_soc.flash_block_data[3];
//
//  double gain_resistance = 4.768 / bq_soc.xemics_to_double(cc_gain);
//  Serial.print("LIB flash_data[0]:                  "); Serial.println(bq_soc.flash_block_data[0], DEC);
//  Serial.print("LIB flash_data[1]:                  "); Serial.println(bq_soc.flash_block_data[1], DEC);
//  Serial.print("LIB flash_data[2]:                  "); Serial.println(bq_soc.flash_block_data[2], DEC);
//  Serial.print("LIB flash_data[3]:                  "); Serial.println(bq_soc.flash_block_data[3], DEC);
//  Serial.print("LIB cc_gain:                        "); Serial.println(cc_gain, DEC);
//  Serial.print("LIB gain_resistance:                "); Serial.println(gain_resistance, DEC);
//  Serial.print("LIB xemics to double (0x7f71205c):  "); Serial.println(bq_soc.xemics_to_double(0x7f71205c), DEC);
//  Serial.print("LIB double to xemics (result ^):    "); Serial.println(bq_soc.double_to_xemics(bq_soc.xemics_to_double(0x7f71205c)), HEX);
//  Serial.print("LIB xemics to double (result ^):    "); Serial.println(bq_soc.xemics_to_double(bq_soc.double_to_xemics(bq_soc.xemics_to_double(0x7f71205c))), DEC);
  Serial.println();
  delay(5000);//delay 2 seconds
}

void bq_unsealed(void) {
  Wire.beginTransmission(0x55);
  Wire.write(0x00); // Control
  Wire.write(0x14);
  Wire.write(0x04);
  Wire.endTransmission();
  
  Wire.beginTransmission(0x55);
  Wire.write(0x00); // Control
  Wire.write(0x72);
  Wire.write(0x36);
  Wire.endTransmission();
}

void bq_full_access(void) {
  Wire.beginTransmission(0x55);
  Wire.write(0x00); // Control
  Wire.write(0xff);
  Wire.write(0xff);
  Wire.endTransmission();
  
  Wire.beginTransmission(0x55);
  Wire.write(0x00); // Control
  Wire.write(0xff);
  Wire.write(0xff);
  Wire.endTransmission();
}

void bq_write_reg(uint8_t addr, uint8_t val) {
    Wire.beginTransmission(0x55);
    Wire.write(addr);
    Wire.write(val);
    Wire.endTransmission(true);
}

void bq_read_flash_block(uint8_t sub_class, uint8_t offset) {
    bq_write_reg(0x61, 0x00); // Block control
    bq_write_reg(0x3e, sub_class); // Flash class
    bq_write_reg(0x3f, offset / 32); // Flash block
    
    Wire.beginTransmission(0x55);
    Wire.write(0x40); // Block data
    Wire.requestFrom(0x55, 32, true);
    for (uint8_t i = 0; i < 32; i++) {
        bq_flash_block_data[i] = Wire.read(); // Data
    }
}

uint8_t bq_flash_block_checksum(void) {
    uint8_t temp = 0;
    for (uint8_t i = 0; i < 32; i++) {
        temp += bq_flash_block_data[i];
    }
    return 255 - temp;
}

double bq_read_cc_gain(void) {
  bq_read_flash_block(104, 0);

  int32_t cc_gain = (int32_t)bq_flash_block_data[0] << 24;
  cc_gain |= (int32_t)bq_flash_block_data[1] << 16;
  cc_gain |= (int32_t)bq_flash_block_data[2] << 8;
  cc_gain |= (int32_t)bq_flash_block_data[3];

  return (double)cc_gain;
}

double bq_read_cc_delta(void) {
  bq_read_flash_block(104, 4);

  int32_t cc_delta = (int32_t)bq_flash_block_data[4] << 24;
  cc_delta |= (int32_t)bq_flash_block_data[5] << 16;
  cc_delta |= (int32_t)bq_flash_block_data[6] << 8;
  cc_delta |= (int32_t)bq_flash_block_data[7];

  return (double)cc_delta;
}

double bq_read_cc_offset(void) {
  bq_read_flash_block(104, 8);

  int16_t cc_offset = (int16_t)bq_flash_block_data[8] << 8;
  cc_offset |= (int16_t)bq_flash_block_data[9];

  return (double)cc_offset;
}

double bq_read_board_offset(void) {
  bq_read_flash_block(104, 10);

  int16_t board_offset = bq_flash_block_data[10];

  return (double)board_offset;
}

uint16_t bq_read_voltage_divider(void) {
  bq_read_flash_block(104, 14);

  int16_t voltage_divider = (int16_t)bq_flash_block_data[14] << 8;
  voltage_divider |= (int16_t)bq_flash_block_data[15];

  return voltage_divider;
}

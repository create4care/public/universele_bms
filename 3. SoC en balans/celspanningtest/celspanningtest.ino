#include <Adafruit_ADS1X15.h>

// ADC definitions
Adafruit_ADS1115 adc;
float cell_1_voltage, cell_2_voltage, cell_3_voltage;
volatile float cell_total_voltage;

#define BALANCE_CELL_1 A2
#define BALANCE_CELL_2 A1
#define BALANCE_CELL_3 A0

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(BALANCE_CELL_1, OUTPUT);
  pinMode(BALANCE_CELL_2, OUTPUT);
  pinMode(BALANCE_CELL_3, OUTPUT);
  digitalWrite(BALANCE_CELL_1, HIGH);
  digitalWrite(BALANCE_CELL_2, HIGH);
  digitalWrite(BALANCE_CELL_3, HIGH);
  adc.begin(0x48);
  delay(2000);
}

void loop() {
  // put your main code here, to run repeatedly:
  measureCellVoltages();
  Serial.println();
  Serial.print("Spanning cel 1: "); Serial.println(cell_1_voltage, 3);
  Serial.print("Spanning cel 2: "); Serial.println(cell_2_voltage, 3);
  Serial.print("Spanning cel 3: "); Serial.println(cell_3_voltage, 3);
  delay(1000);
}

void measureCellVoltages(void) {
  cell_1_voltage = adc.computeVolts(adc.readADC_SingleEnded(2));
  cell_2_voltage = adc.computeVolts(adc.readADC_SingleEnded(1));
  cell_3_voltage = adc.computeVolts(adc.readADC_SingleEnded(0));
}

/*
 * volatile variabele: https://www.arduino.cc/reference/en/language/variables/variable-scope-qualifiers/volatile/
 * maak een variabele volatile wanneer deze door een interrupt gewijzigd kan worden, zodat de variabele steeds opnieuw wordt uitgelezen en de uC niet denkt dat hij weet wat de waarde van de variabele is.
 */

/*
 * TO DO:
 * - write code for situations were the expected battery_capacity is higher dan in reality
 * - celspanningen en temp naar uart sturen
 */

/* 
 *  NOTES:
 *  - do not store current_battery_capacity in EEPROM, since this will reduce EEPROM life significantly
 *    instead, store state_of_charge and battery_capacity; those variables are changed less often
 *    IF POSSIBLE store often changing variables in other type of storage, to extend total product life
 */

/*
 * VOORGESTELDE HARDWARE VERANDERINGEN:
 * - LEDS dimmen met PWM pinnen is prima, maar dan wel meer PWM pinnen nodig. Nu kan er geen timer meer gebruikt worden, omdat ze allemaal aan de PWM pinnen gekoppeld zitten
 */

#include <Adafruit_ADS1X15.h>
#include <INA.h>
#include <util/atomic.h>
#include <EEPROM.h> // NOTE: EEPROM is erased when it is flashed via ISP header

// I/O pin definitions
#define LED_1 11
#define LED_2 10
#define LED_3 9
#define LED_4 6
#define LED_5 5
#define LDR A0
#define ADC_ALERT 2
#define INA_ALERT 3

// ADC definitions
Adafruit_ADS1115 adc;
float cell_1_voltage, cell_2_voltage, cell_3_voltage;
volatile float cell_total_voltage;

// INA definitions
INA_Class ina(1);
#define INA_CONVERSION_TIME 8244 // in microseconds
#define INA_AVERAGING 128
uint16_t INA_CONVERSION_DURATION = 2 * (INA_CONVERSION_TIME / 1000.0) * INA_AVERAGING; // in milliseconds, don't care about the rounding error

// for testing purposes...
volatile int32_t current_battery_capacity = 0; // uAh
volatile int32_t battery_capacity = 2000000; // uAh
volatile uint8_t state_of_charge = 0; //every LSB represents 0.5%, so this variable contains a number between 0 (0%) and 200 (100%)
volatile uint8_t battery_temperature = 0;
uint32_t previous_millis = 0;
uint32_t previous_millis_ledbar = 0;
bool last_ledbar_state = LOW;
#define UPDATE_INTERVAL 10000 // in milliseconds
int32_t busMilliAmps; // global for debugging purposes; to uartbridge
volatile int32_t used_energy;
const uint32_t EEPROM_DATA_CHANGED_FLAG = 0x23; // change to reset user variables. When sketch uploaded via ISP not neccesary, since EEPROM is cleared

union Union_buffer {
  int32_t number;
  byte bytes[4];
};

union Union_float_buffer {
  float number;
  byte bytes[4];
};

Union_buffer battery_capacity_buffer;
Union_buffer current_battery_capacity_buffer;
Union_buffer busMilliAmps_buffer;
Union_buffer used_energy_buffer;
Union_float_buffer cell_1_voltage_buffer;
Union_float_buffer cell_2_voltage_buffer;
Union_float_buffer cell_3_voltage_buffer;
Union_float_buffer cell_total_voltage_buffer;

void setup() {
  /*
   * EEPROM adresses
   * 0: flag: if set to EEPROM_DATA_CHANGED_FLAG, there are variables written to EEPROM
   * 1: battery_capacity (MSB)
   * 2: battery_capacity
   * 3: battery_capacity
   * 4: battery_capacity (LSB)
   * 5: state_of_charge
   */
   
//  EEPROM.write(0, EEPROM_DATA_CHANGED_FLAG);
  if(EEPROM.read(0) == EEPROM_DATA_CHANGED_FLAG) { // data exists in EEPROM 
    EEPROM.get(1, battery_capacity);      
    state_of_charge   = EEPROM.read(5);
    current_battery_capacity = (float)(battery_capacity * (float)(state_of_charge / 200.0));
  } else {
    current_battery_capacity = battery_capacity; // expecting the battery is full
    state_of_charge = 200; // if the battery is full, the SoC is 100%  
  }

  
  
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(LED_3, OUTPUT);
  pinMode(LED_4, OUTPUT);
  pinMode(LED_5, OUTPUT);
  pinMode(LDR, INPUT);
  pinMode(ADC_ALERT, INPUT_PULLUP);
  pinMode(INA_ALERT, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ADC_ALERT), adcAlertInterrupt, FALLING);
  attachInterrupt(digitalPinToInterrupt(INA_ALERT), inaAlertInterrupt, FALLING);

  digitalWrite(LED_3, HIGH);

  adc.begin(0x48);                            // ADC is on address 0x48

  ina._EEPROM_offset = 32;                    // Reserve some bytes for storing other variables
  ina.begin(10, 8000);                        // Expecting max 10A. The shunt resitor has a value of 8mOhm or 8000uOhm.
  ina.setBusConversion(8244);                 // Maximum conversion time 8.244ms
  ina.setShuntConversion(8244);               // Maximum conversion time 8.244ms
  ina.setAveraging(128);                      // Average each reading n-times
  ina.setMode(INA_MODE_CONTINUOUS_BOTH);      // Bus/shunt measured continuously
  ina.alertOnConversion(true);                // Make alert pin go low on finish
}

void loop() {
  if(millis() - previous_millis > UPDATE_INTERVAL) {
    previous_millis = millis();
//    measureCellVoltages();
    state_of_charge = (float)(current_battery_capacity / (float)battery_capacity) * 200;
//    safeVariablesToEEPROM();
//    sendDataToUno(); // 
  }
  
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    // code with interrupts blocked (consecutive atomic operations will not get interrupted)
    // in this atomic block, interrupts are disabled.
    // for example, you don't want to read or write a variable greater dan 8-bits when an interrupt occurs.
    // it could be possible that the variable you are reading is changed by the ISR
  }

  if(millis() - previous_millis_ledbar > 250) {
    previous_millis_ledbar = millis();
    updateLEDbar();
  } 
}

void updateLEDbar(void) {
  int brightness = 1024 - analogRead(LDR)/4;
  if(state_of_charge >= 2*0 && state_of_charge < 2*10) {
    if(last_ledbar_state == LOW) {
      analogWrite(LED_5, brightness);
      last_ledbar_state = HIGH;
    } else {
      digitalWrite(LED_5, LOW);
      last_ledbar_state = LOW;
    }   
    digitalWrite(LED_4, LOW);
    digitalWrite(LED_3, LOW);
    digitalWrite(LED_2, LOW);
    digitalWrite(LED_1, LOW);
  }
  if(state_of_charge >= 2*10 && state_of_charge < 2*30) {
    analogWrite(LED_5, brightness);
    digitalWrite(LED_4, LOW);
    digitalWrite(LED_3, LOW);
    digitalWrite(LED_2, LOW);
    digitalWrite(LED_1, LOW);
  }
  if(state_of_charge >= 2*30 && state_of_charge < 2*50) {
    analogWrite(LED_5, brightness);
    analogWrite(LED_4, brightness);
    digitalWrite(LED_3, LOW);
    digitalWrite(LED_2, LOW);
    digitalWrite(LED_1, LOW);
  }
  if(state_of_charge >= 2*50 && state_of_charge < 2*70) {
    analogWrite(LED_5, brightness);
    analogWrite(LED_4, brightness);
    analogWrite(LED_3, brightness);
    digitalWrite(LED_2, LOW);
    digitalWrite(LED_1, LOW);
  }
  if(state_of_charge >= 2*70 && state_of_charge < 2*90) {
    analogWrite(LED_5, brightness);
    analogWrite(LED_4, brightness);
    analogWrite(LED_3, brightness);
    analogWrite(LED_2, brightness);
    digitalWrite(LED_1, LOW);
  }
  if(state_of_charge >= 2*90 && state_of_charge <= 2*100) {
    analogWrite(LED_5, brightness);
    analogWrite(LED_4, brightness);
    analogWrite(LED_3, brightness);
    analogWrite(LED_2, brightness);
    analogWrite(LED_1, brightness);
  }
}

void measureCellVoltages(void) {
  cell_1_voltage = adc.computeVolts(adc.readADC_SingleEnded(2));
  cell_2_voltage = adc.computeVolts(adc.readADC_SingleEnded(1));
  cell_3_voltage = adc.computeVolts(adc.readADC_SingleEnded(0));
//  cell_total_voltage = ina.getBusMilliVolts() / 1000.0; // MEasure voltage in interrupt
}

uint8_t measureBatteryTemperature(void) {
  double voltage = adc.computeVolts(adc.readADC_SingleEnded(3));
  double temperature = -55.197 * pow(voltage, 4) + 366.78 * pow(voltage, 3) - 916.32 * pow(voltage, 2) + 972.69 * voltage - 302.17;
  return (uint8_t)temperature; // in degrees Celsius
}

void adcAlertInterrupt(void) {
  
}

void inaAlertInterrupt(void) {
  sei(); // enable interrupt for I2C, so we can send I2C messages in the interrupt
//  static int16_t busMilliAmps = ina.getBusMicroAmps() / 1000.0;
  busMilliAmps = ina.getBusMicroAmps() / 1000.0; // for debuging to UART
  cell_total_voltage = ina.getBusMilliVolts() / 1000.0;

//  static int32_t used_energy = (int32_t)(busMilliAmps * INA_CONVERSION_DURATION); // mAms
  used_energy = (int32_t)(busMilliAmps * INA_CONVERSION_DURATION); // mAms, for debugging UART

  current_battery_capacity -= used_energy / 3600; // uAh

  // handle situations were the battery_capacity is lower than reality
  if(current_battery_capacity < 0) {                    // it is not possible that this value becomes negative, because it is derived from the battery_capacity
    battery_capacity += abs(current_battery_capacity);  // but when it becomes negative, the battery_capacity value is not correct, so enlarge it
    current_battery_capacity = 0;
  }
  if(current_battery_capacity > battery_capacity) {     // this happens when the charger put more energy in the battery than the expected battery_capacity
    battery_capacity = current_battery_capacity;        // the battery_capacity is at least as big as the current_battery_capacity
  }

  // handle situation were the battery_capacity is higher than reality
  // TO DO

  EEPROM.update(0, state_of_charge);

  measureCellVoltages();
  battery_temperature = measureBatteryTemperature();
  sendDataToUno();
  safeVariablesToEEPROM();
  
  ina.waitForConversion(); // No need to wait for conversion since the Alert pin indicates it is ready, but this function resets the Alert pin.
}

void sendDataToUno() {
  battery_capacity_buffer.number = battery_capacity;
  current_battery_capacity_buffer.number = current_battery_capacity;
  busMilliAmps_buffer.number = busMilliAmps;
  used_energy_buffer.number = used_energy;
  cell_1_voltage_buffer.number = cell_1_voltage;
  cell_2_voltage_buffer.number = cell_2_voltage;
  cell_3_voltage_buffer.number = cell_3_voltage;
  cell_total_voltage_buffer.number = cell_total_voltage;
  
  Wire.beginTransmission(4); // transmit to device #4
  Wire.write(battery_capacity_buffer.bytes, 4);
  Wire.write(current_battery_capacity_buffer.bytes, 4);
  Wire.write(state_of_charge);
  Wire.write(busMilliAmps_buffer.bytes, 4);
//  Wire.write(used_energy_buffer.bytes, 4);
  Wire.write(cell_1_voltage_buffer.bytes, 4);
  Wire.write(cell_2_voltage_buffer.bytes, 4);
  Wire.write(cell_3_voltage_buffer.bytes, 4);
  Wire.write(cell_total_voltage_buffer.bytes, 4); // more than 32 bytes at ones seems to be too much
  Wire.write(battery_temperature);
  Wire.endTransmission();   // stop transmitting
}

void safeVariablesToEEPROM(void) {
  EEPROM.update(0, EEPROM_DATA_CHANGED_FLAG);
  EEPROM.put(1, battery_capacity); // 32 bit value, spread over addresses 1,2,3,4
  EEPROM.update(5, state_of_charge);
}

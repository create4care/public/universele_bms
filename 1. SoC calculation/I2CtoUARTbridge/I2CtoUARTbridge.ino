#include <Wire.h>

uint8_t state_of_charge;
uint8_t battery_temperature;

union Union_buffer {
  int32_t number;
  byte bytes[4];
};

union Union_float_buffer {
  float number;
  byte bytes[4];
};

Union_buffer battery_capacity_buffer;
Union_buffer current_battery_capacity_buffer;
Union_buffer busMilliAmps_buffer;
Union_buffer used_energy_buffer;
Union_float_buffer cell_1_voltage_buffer;
Union_float_buffer cell_2_voltage_buffer;
Union_float_buffer cell_3_voltage_buffer;
Union_float_buffer cell_total_voltage_buffer;

void setup()
{
   Wire.begin(4);        // join i2c bus 
   Wire.onReceive(receiveEvent); // register event
   Serial.begin(115200);  // start serial for output
   Serial.println("START");
}

void loop()
{   
   delay(1000);
}

void receiveEvent(int howMany)
{
  while(0 < Wire.available()) // loop through all 
  {
    battery_capacity_buffer.bytes[0] = Wire.read();
    battery_capacity_buffer.bytes[1] = Wire.read();
    battery_capacity_buffer.bytes[2] = Wire.read();
    battery_capacity_buffer.bytes[3] = Wire.read();
    current_battery_capacity_buffer.bytes[0] = Wire.read();
    current_battery_capacity_buffer.bytes[1] = Wire.read();
    current_battery_capacity_buffer.bytes[2] = Wire.read();
    current_battery_capacity_buffer.bytes[3] = Wire.read();
    state_of_charge = Wire.read();
    busMilliAmps_buffer.bytes[0] = Wire.read();
    busMilliAmps_buffer.bytes[1] = Wire.read();
    busMilliAmps_buffer.bytes[2] = Wire.read();
    busMilliAmps_buffer.bytes[3] = Wire.read();
//    used_energy_buffer.bytes[0] = Wire.read();
//    used_energy_buffer.bytes[1] = Wire.read();
//    used_energy_buffer.bytes[2] = Wire.read();
//    used_energy_buffer.bytes[3] = Wire.read();
    cell_1_voltage_buffer.bytes[0] = Wire.read();
    cell_1_voltage_buffer.bytes[1] = Wire.read();
    cell_1_voltage_buffer.bytes[2] = Wire.read();
    cell_1_voltage_buffer.bytes[3] = Wire.read();
    cell_2_voltage_buffer.bytes[0] = Wire.read();
    cell_2_voltage_buffer.bytes[1] = Wire.read();
    cell_2_voltage_buffer.bytes[2] = Wire.read();
    cell_2_voltage_buffer.bytes[3] = Wire.read();
    cell_3_voltage_buffer.bytes[0] = Wire.read();
    cell_3_voltage_buffer.bytes[1] = Wire.read();
    cell_3_voltage_buffer.bytes[2] = Wire.read();
    cell_3_voltage_buffer.bytes[3] = Wire.read();
    cell_total_voltage_buffer.bytes[0] = Wire.read();
    cell_total_voltage_buffer.bytes[1] = Wire.read();
    cell_total_voltage_buffer.bytes[2] = Wire.read();
    cell_total_voltage_buffer.bytes[3] = Wire.read();
    battery_temperature = Wire.read();

    
    Serial.print("Batterijcapaciteit               "); Serial.print(battery_capacity_buffer.number, DEC);         Serial.print(" uAh    =   "); Serial.print((float)(battery_capacity_buffer.number / 1000.0), 3);         Serial.println(" mAh");
    Serial.print("Resterende batterijcapaciteit:   "); Serial.print(current_battery_capacity_buffer.number, DEC); Serial.print(" uAh    =   "); Serial.print((float)(current_battery_capacity_buffer.number / 1000.0), 3); Serial.println(" mAh");
    Serial.print("State-of-Charge:                 "); Serial.print((float)(state_of_charge/2.0), 1);             Serial.println(" %");
    Serial.print("Huidige stroom:                  "); Serial.print(busMilliAmps_buffer.number, DEC);             Serial.println(" mA");
//    Serial.print("Gebruikte energie (deze meting): "); Serial.print(used_energy_buffer.number, DEC);              Serial.print(" mAms    =   "); Serial.print((float)(used_energy_buffer.number/3600.0), 3);                  Serial.print(" uAh   =   "); Serial.print((float)(used_energy_buffer.number/3600000.0), 3); Serial.println(" mAh");
    Serial.print("Spanning cel 1:                  "); Serial.print(cell_1_voltage_buffer.number, 3);             Serial.println(" V");
    Serial.print("Spanning cel 2:                  "); Serial.print(cell_2_voltage_buffer.number, 3);             Serial.println(" V");
    Serial.print("Spanning cel 3:                  "); Serial.print(cell_3_voltage_buffer.number, 3);             Serial.println(" V");  
    Serial.print("Batterijspanning:                "); Serial.print(cell_total_voltage_buffer.number, 3);         Serial.println(" V");
    Serial.print("Batterijtemperatuur:             "); Serial.print(battery_temperature);                         Serial.println(" °C");
    Serial.println("--------------------------------------------------------------------------------");
  }
}
